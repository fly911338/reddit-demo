# reddit-demo



## Generate key

<code>
keytool -genkey -v -validity 10000 -alias reddit -keyalg RSA -keystore reddit.jks -keysize 2048 -storetype JKS
</code>
<br>
and then input your password

## Reference
https://programmingtechie.com
<br>
https://www.youtube.com/channel/UCD20RZV_WHQImisCW2QZwDw
