package com.kevin.redditdemo.repository;


import com.kevin.redditdemo.model.Post;
import com.kevin.redditdemo.model.User;
import com.kevin.redditdemo.model.Vote;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface VoteRepository extends JpaRepository<Vote, Long> {
    Optional<Vote> findTopByPostAndUserOrderByVoteIdDesc(Post post, User currentUser);
}
