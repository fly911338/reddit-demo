package com.kevin.redditdemo.repository;


import com.kevin.redditdemo.model.Post;
import com.kevin.redditdemo.model.Subreddit;
import com.kevin.redditdemo.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PostRepository extends JpaRepository<Post, Long> {
    List<Post> findAllBySubreddit(Subreddit subreddit);

    List<Post> findByUser(User user);
}
