package com.kevin.redditdemo.repository;


import com.kevin.redditdemo.model.Comment;
import com.kevin.redditdemo.model.Post;
import com.kevin.redditdemo.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CommentRepository extends JpaRepository<Comment, Long> {
    List<Comment> findByPost(Post post);

    List<Comment> findAllByUser(User user);
}
